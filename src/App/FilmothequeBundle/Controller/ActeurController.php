<?php

namespace App\FilmothequeBundle\Controller;

use App\FilmothequeBundle\Entity\Acteur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ActeurController extends Controller
{
    /**
     * Lists all acteur entities.
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $acteurs = $em->getRepository('AppFilmothequeBundle:Acteur')->findAll();

        return $this->render('AppFilmothequeBundle:acteur:index.html.twig', array(
            'acteurs' => $acteurs,
        ));
    }

    /**
     * Creates or Edit an acteur entity.
     */
    public function editerAction(Request $request, $id = null)
    {
        $message    = '';
        $em         = $this->getDoctrine()->getManager();

        if (isset($id)) {
            // Modification d'un acteur existant : on recherche ses données
            $acteur = $em->find('AppFilmothequeBundle:Acteur', $id);

            if (!$acteur) {
                $message = 'Aucun acteur trouvé';
            }
        }
        else {
            // Ajout d'un nouvel acteur
            $acteur = new Acteur();
        }

        $form = $this->createForm('App\FilmothequeBundle\Form\ActeurType', $acteur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($acteur);
            $em->flush();

            if (isset($id)) {
                $message = 'Acteur modifié avec succès !';
            }
            else {
                $message = 'Acteur ajouté avec succès !';
            }
        }

        if (isset($id)) {
            $acteur = $em->find('AppFilmothequeBundle:Acteur', $id);
            $deleteForm = $this->createDeleteForm($acteur);

            return $this->render('AppFilmothequeBundle:acteur:editer.html.twig', array(
                'acteur'      => $acteur,
                'message'     => $message,
                'form'        => $form->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }
        else {
            return $this->render('AppFilmothequeBundle:acteur:editer.html.twig', array(
                'message'     => $message,
                'form'        => $form->createView(),
            ));
        }
    }

    /**
     * Finds and displays a acteur entity.
     */
    public function showAction(Acteur $acteur)
    {
        $deleteForm = $this->createDeleteForm($acteur);

        return $this->render('AppFilmothequeBundle:acteur:show.html.twig', array(
            'acteur' => $acteur,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a acteur entity.
     */
    public function deleteAction(Request $request, Acteur $acteur)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createDeleteForm($acteur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($acteur);
            $em->flush();
        }
        else {
            $em->remove($acteur);
            $em->flush();
        }

        return $this->redirectToRoute('acteur_index');
    }

    /**
     * Creates a form to delete a acteur entity.
     *
     * @param Acteur $acteur The acteur entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Acteur $acteur)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('acteur_delete', array('id' => $acteur->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function topAction($max = 3)
    {
        $em = $this->getDoctrine()->getManager();
        $acteurs = $em->getRepository('AppFilmothequeBundle:Acteur')->getJeunesActeurs($max);

        return $this->render(
            'AppFilmothequeBundle:acteur:liste.html.twig',
            array('acteurs' => $acteurs,)
        );
    }
}
