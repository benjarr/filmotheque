<?php

namespace App\FilmothequeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('AppFilmothequeBundle:Categorie')->findAll();

        return $this->render('AppFilmothequeBundle:Default:index.html.twig', array(
            'categories' => $categories
        ));
    }
}
