<?php

namespace App\FilmothequeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActeurType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, array('label' => 'Nom de famille'))
            ->add('prenom', TextType::class, array('label' => 'Prénom'))
            ->add('dateDeNaissance', BirthdayType::class, array(
                'label'   => 'Date de naissance',
                'format'  => 'ddMMyyyy',
            ))
            ->add('sexe', ChoiceType::class, array(
                'choices' => array('Féminin' => 'F', 'Masculin' => 'M')
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\FilmothequeBundle\Entity\Acteur'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_filmothequebundle_acteur';
    }


}
