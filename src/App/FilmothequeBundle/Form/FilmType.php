<?php

namespace App\FilmothequeBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilmType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('description')
            ->add('categorie', EntityType::class, array(
                'class' => 'App\FilmothequeBundle\Entity\Categorie',
                'placeholder'  => 'Choisir une catégorie',
                'choice_label' => 'nom',
            ))
            ->add('acteurs', EntityType::class, array(
                'class'        => 'App\FilmothequeBundle\Entity\Acteur',
                'choice_label' => 'PrenomNom',
                'expanded'     => true,
                'multiple'     => true,
                'required'     => false,
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\FilmothequeBundle\Entity\Film'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_filmothequebundle_film';
    }
}
